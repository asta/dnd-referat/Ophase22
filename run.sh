curl -o website.zip "https://cloud.asta.uni-goettingen.de/s/x2N9GecpyHb6zNo/download"
unzip website.zip
mkdir input
rm -r input/*
cp -R website/* input/
python3 build.py
rm website.zip
rm -r website 
