# O-Phasen website 2023

Das Repository beinhaltet das Script um den inhalt eines CloudOrdners in die Ophasen Website und den digitalen ersti beutel umwandelt.

## Vorbereitung

Auf dem Server ( zurzeut asta-astamain) muss das repository geclont werden und die Bibliotheken und Hilfsfunktionen installiert werden. Außerdem muss ein Ordner in der Serverconfig vorbereitet werden in dem die erstellten HTML Dateien Kopiert werden können.(zur zeit `/var/www/ophase2022`)

### Bibs

zum installieren (eventuell unvollständig)

```bash
apt-get install poppler-utils libfile-mimeinfo-perl libimage-exiftool-perl ghostscript libsecret-1-0 zlib1g-dev libjpeg-dev imagemagick libmagic1 webp
```

```bash
pip install Jinja2 jinja-markdown PyYAML colorthief preview-generator
```

## Workflow

### Inhalte erstellen

Mit den ophasen cloud account(in passwords.asta.uni-goettingen.de) auf https://cloud.asta.uni-goettingen.de/apps/files anmelden. Die Files app ist versteckt und die URL muss händisch eingegeben werden. Dort können Inhlate bearbeite werden(siehe unten)

### Updaten

1. auf Server anmelden und ins Repository navigieren
  
2. `bash run.sh` ausführen
  
3. Output in Server Ordner kopieren z.B. `sudo cp -r out/* /var/www/ophase2022`
  

## Hauptseite

Auf der Hauptseite können das Vorwort und die Veranstaltungen bearbeitet werden.

**Das Vorwort** kann einfach in der Datei `Website/Vorwort.md` bearbeitet werden, in einfacher Markdown syntax.

**Die Veranstaltungen** sind in einzelnen Dateien gespeichert im Ordner`Website/Events/`. Die Namen der Dateien sind eigentlich egal aber für die Übersicht...

Diese Dateien Sind Markdown datein mit YAML header,<mark> das heißt sie müssen mit dem besseren Markdown Editor in der Cloud bearbeitet werden. </mark>

Dafür muss auf die Drei punkte neben der Datei geklickt werden und dann auf "Bearbeiten im Nur-Text-Editor" geklickt werden.

Der Header enthält die attribute, title, datum, start und end, jedoch kann end auch für den Ort verwendet werden. Bin nicht mehr dazu gekommen das schöner zu machen.

z.B:

```yaml
---
title: CheersQueers-Kneipen Abend
datum: 2022-10-21
start: "19:00 "
end: " Stilbrvch"
---


event Text
```

Nach dem Header kann einfacher Markdown code geschrieben werden.

Zum hinzufügen einer neuen Veranstaltung muss einfach nur eine Neue datei angelegt werde.

## Beutel

Grundsätzlich ist der Beutel so gestaltet das kacheln für URL's und PDF's erstellt werden können. Für PDF's kann ein Preview Image erstellt werden, für URL's muss ein bild mit hochgeladen werden. Alle Kacheln werden in `website/beutel.yaml` gespeichert. Jeder Eintrag beginnt mit einem Spiegel strich und der Rest muss eingerückt werden. Für PDF's ist nur Name und link zwingend notwendig, jedoch kann eine category gewählt werden(steht dann in der zweiten Zeile der Kachel) und ein img_link wenn nicht das normale Preview verwendet werden soll. Für Webseiten ist der img_link zwingend notwendig da es keine Preview Funktion gibt.

Die PDF's müssen in den `website/files` Ordner hochgeladen werden, bitte denkt dran alte files zu löschen damit das ganze nicht riseig wird.

```yaml
-
  name: AStA
  category: AStA
  link: https://asta.uni-goettingen.de
  img_link: files/AStA.png
-
  name: Digitale Stadtführung
  img_link: files/stadtRundgang.png
  link: https://stadtfuehrung.asta.uni-goettingen.de/
-
  name: TV-Stud
  link: files/tv-stud.pdf
-
  name: Radikarla
  link: files/radikarla.pdf
  img_link: files/radikarla.pdf.png
```

## Theme

Das Layout und damit auch der Title können in diesem Git unter `Templates`geändert werden. Das Template ist in jinja2 geschrieben.

Änderungen im Theme werden auch mit dem update workflow von oben übernommen
