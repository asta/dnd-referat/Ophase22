FROM ubuntu:latest
COPY . /app
WORKDIR /app
RUN apt clean 
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y nginx jq bash python3 python3-pip poppler-utils libfile-mimeinfo-perl libimage-exiftool-perl ghostscript libsecret-1-0 zlib1g-dev libjpeg-dev imagemagick libmagic1 webp curl unzip
RUN pip install jinja2 jinja_markdown PyYAML colorthief preview-generator
RUN curl -o website.zip "https://cloud.asta.uni-goettingen.de/s/x2N9GecpyHb6zNo/download"
RUN unzip website.zip
RUN mkdir input
RUN cp -R website/* /app/input/
RUN python3 /app/build.py
COPY out /var/www/html/
RUN cp -R out/* /var/www/html/
EXPOSE 80

ENTRYPOINT /bin/bash -c "/usr/sbin/nginx -t -g 'daemon off; master_process on;' && /usr/sbin/nginx -g 'daemon off; master_process on;'"

