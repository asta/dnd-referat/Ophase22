from os import listdir
from jinja2 import Environment, FileSystemLoader
from jinja_markdown import MarkdownExtension
import yaml
import shutil
from distutils.dir_util import copy_tree
from colorthief import ColorThief
from preview_generator.manager import PreviewManager

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)
env.add_extension(MarkdownExtension)
env.add_extension('jinja2.ext.do')

index = env.get_template('index.html')
beutel = env.get_template('Beutel.html')
with open('input/Vorwort.md','r') as v:
    vorwort = v.read()

with open('input/BeutelVorwort.md','r') as bv:
    beutelVorwort = bv.read()


vas=[]
for file in listdir("input/Events"):
    with open("input/Events/"+file,'r') as f:
        inp  = f.read()
        inp = inp.split("---")
        header = inp[1]
        content=inp[2]
        event=yaml.safe_load(header)
        event['text']=content
        vas.append(event)
vas.sort(key=lambda x: (x['datum'],x['start']))
output = index.render(Jahr="2023",Vorwort=vorwort,verantstaltungen=vas)
with open("out/index.html","w") as out:
    out.write(output)

with open("input/beutel.yaml") as f:
    inhalt=yaml.safe_load(f.read())

cache_path ='out/img_tmp'
manager = PreviewManager(cache_path, create_folder= True)
copy_tree("templates/css","out/css")
copy_tree("input/files","out/files")

for inh in inhalt:
    if 'http' not in inh['link'] and 'img_link' not in inh.keys() :
        inh['img_link']=manager.get_jpeg_preview("input/"+inh['link'], width=500, height=500)
        inh['img_link']= inh['img_link'][4:]

    inh['farbe']= ColorThief('out/'+inh['img_link']).get_palette(color_count=6)
  #  inh['farbe']=((inh['farbe'][0][0]+inh['farbe'][1][0]+inh['farbe'][2][0])/3,(inh['farbe'][0][1]+inh['farbe'][1][1]+inh['farbe'][2][1])/3,(inh['farbe'][0][2]+inh['farbe'][1][2]+inh['farbe'][2][2])/3)
    inh['farbe']= inh['farbe'][1]
    inh['textFarbe']=(255-inh['farbe'][0],255-inh['farbe'][1],255-inh['farbe'][2])
    


output = beutel.render(Jahr="2023",Vorwort=beutelVorwort,inhalt=inhalt)


with open("out/Beutel.html","w") as out:
    out.write(output)

